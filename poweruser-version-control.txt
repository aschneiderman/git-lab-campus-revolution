Do Power Users Use Version Control?

Based on my experience supporting power users, the experience of people I know
who support power users, and from what I've read in articles, on social media, etc., 
aside from power users who primarily use open source tools such as Jupyter Notebooks, 
using version control doesn't seem to be very common for power users. But how 
about some data to back up this claim?

It's estimated that there are between 650 and 750 million Excel users 
worldwide. Even if only 10% are power users, that's more people than all of 
the users for GitHub and GitLab combined.

To get a more accurate estimate, you could analyze GitLab repos. Since most 
power users spend most of their time working with data they can't make public,
you could find out what percentage and absolute number of users who have a 
nonprofit or corporate email address also have a private repo that contains 
an Excel or PowerPoint file.

But there's a simpler way to get a rough answer. Excel and PowerPoint can be a 
little fussy if they are being used on an open source platform. So, how many
people have posted a question about Excel or PowerPoint in the GitLab forums? 
I found 6 for Excel, 1 for PowerPoint. If most power users are using version 
control, there is no way these numbers would be so low.
