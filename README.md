# Sparking a Git(Lab) Revolution on Campus

How can GitLab become the tool of choice at colleges and universities? One way to do it: make a big, bold statement that changes the conversation on campus. Here's an example of how.  

### What Will It Take so Everyone Can Contribute?

According to the [About GitLab](https://about.gitlab.com/company/#targetText=GitLab%20believes%20in%20a%20world,-only%20to%20read-write) page,

> GitLab believes in a world where everyone can contribute. Our mission is to change all creative work from read-only to read-write.  

That's an incredibly inspiring vision. But pulling it off is easier said than done.

Take Git. Git is the front door to contributing in the open source world. And on Windows, it's a hot mess. As a result, for example, even as more and more non-technical staff in large organizations are able to do a little data wrangling and analysis, these power users [rarely use version control](poweruser-version-control.txt ). 

And if we want to truly reach everyone --  the  activist janitors whose dues paid my salary when I worked for the Service Employment International Union (SEIU) and who may lose their jobs to robots, the low-paid "care workers" who were there for my mom in the final months of her life this year, the coal miners whose jobs must be eliminated if we're going to stop climate change -- it's an even steeper climb. As I can tell you from decades of working in the community, it is possible to train some of these folks to use Git, but most find Git intimidating and painful.

Git's user-unfriendliness is a symptom of a broader problem. Many open source coders are deeply passionate about democratizing the technology they build. But the tech world [suffers from self-selection bias](https://toolkit.makersall.org/pages/30-smooth/10-culture-community-coding-UX.html): too many people in our field don't realize that what's easy for them and the developers they know is a serious obstacle for many other people.

GitLab is in a unique position to change this. GitLab could spearhead a revolution to get the tech world to embrace what it means to truly democratize technology, to build a world where everyone really could contribute. And the road to that revolution begins on college campuses.

### Redesigning Git to Kickstart a Read-Write Revolution on Campus 

Because Git is at the heart of the read-write revolution, making it more accessible is a good place to start. Trying to make Git accessible for janitors, home care workers, etc is a pretty intimidating first step. But college students who aren't techies? That's much more manageable.  And when it comes to teaching tech, although there is a gap between teaching non-technical white-collar workers and blue-collar workers, in my experience it isn't as large as you might expect.  Finally, if you want to pick a group who tend to be undaunted by the idea of tackling a big challenge, you couldn't pick better partners than college students. 

So, GitLab could announce that we are sponsoring an undergraduate and graduate student design contest to re-envision Git for nontechnical college students. Makers of the top designs will win a little cash and a paid internship at GitLab. During these internships, the students would:
- Work collaboratively with other interns and GitLab staff on developing an MVP of a redesigned Git
- Figure out next steps -- e.g., how do we test, get feedback on, and refine our MVP?  How & when do we iteratively expand folks in the conversation so we can eventually make Git truly accessible to all?
- Begin to brainstorm how down the road we might make the other methodologies supported by GitLab -- e.g., scrum, micro services, continuous deployment, etc. -- more accessible

A few more thoughts about the contest:
- We might consider running the contest the [the way you do everything at GitLab](https://about.gitlab.com/handbook/values/): start by creating a quick and dirty contest proposal and quickly iterate it.  In other words, we'd turn designing the contest into an opportunity to start conversations and build new relationships with faculty and student groups in design, computer science, journalism, the social sciences, etc.  Instead of our first interaction with them being a product pitch, we are asking their opinion and offering them a way to help make tech more accessible.
- To increase the odds of a diverse group of contestants, one focus of our outreach should be student and faculty organizations for underrepresented groups. It would be especially important to do so early on so a diverse group of voices could help shape the contest and what happens afterwards; a diverse group of contest judges would also be critical.
- We should encourage student contestants to be bold, creative, and adventurous (personally, I'm hoping for a US Southern-Style "[Git R Done](https://www.urbandictionary.com/define.php?term=Git-R-Done)" version). If we want to make Git not only unintimidating to beginners but a joy to use -- i.e., [Don Norman](https://www.nngroup.com/articles/definition-user-experience) meets [Marie Kondo](https://shop.konmari.com/pages/about) -- I believe we are better off starting from a bit out there and pruning our way back to a "[boring solution](https://gitlab.com/gitlab-com/blog-posts/issues/31)." As we do so, we might even spark conversations such as, do we want to eventually make the entire creative tool chain accessible and a joy to use, making GitLab as easy and enjoyable to make our own as our smart phones?


### Impact

At a minimum, this contest would introduce GitLab to many people on college campuses who don't know much about it and would position it not only as a great alternative to, say, GitHub, but also as an opportunity to be part of a movement. The contest would also a great way to foster the development of GitLab's evangelists, both as a way of helping GitLab and as a way of giving campus evangelists an opportunity to build connections for their careers. And it would create opportunities for recruiting new GitLab staff, including recruiting from a more diverse pool of candidates.

And that's just the payoff if the contest is only moderately successful. 

If the contest really takes off, it could be an opportunity for GitLab to open up new possibilities for people around the globe to improve their lives. In the next 20 years, robots/AI, augmented and virtual reality, digital fabrication, and other emerging tech that will create an abundance of wealth. As it stands now, most people won't get a chance to fully contribute to and benefit economically from this abundance. But if GitLab can use this contest as a lever to crack the door open, we can take our first step along the road of giving the people our society has abandoned a real shot at using their creativity to forge a better future for themselves and their families -- and build an amazing future for GitLab.

&nbsp;

&nbsp;


### Notes
- I don't know enough about your company's plans and culture to know whether this proposal, which is admittedly over-the-top, would make sense for GitLab to take on. If it's not a good fit, hopefully it can serve as a thought experiment that might inspire other ideas for giving GitLab's already powerful product another competitive advantage -- and doing a little good in the process.
- FYI, the ideas in this proposal were inspired in part by a report I published this Spring called, [Makers All: How to Help Communities Transform Emerging Tech so They Can Shape Their Destinies](https://toolkit.makersall.org).  
